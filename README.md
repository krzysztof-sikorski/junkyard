# Junkyard

Archive of abandoned projects.

**NOTE:**
Repository history is periodically squashed or rebased.
I do not wish to preserve minute details of project development.

# Repository mirrors

- [Codeberg](https://codeberg.org/krzysztof-sikorski/junkyard) (main instance)
- [Disroot](https://git.disroot.org/krzysztof-sikorski/junkyard) (backup)

# Licensing

Each project has its own licence. Check project directories for details.
